# README #

### General Information ###

* Search Engines project of Datex Group
* Version 1.00
* https://bitbucket.org/upd-dei-stud-prj/seupd2021-dtx/src/master/
* Group members
	* Anahita Abbaspour (anahita.abbaspour@studenti.unipd.it)
	* Alessandro Benetti (alessandro.benetti.1@studenti.unipd.it)
	* Emanuela Pozzer (emanuela.pozzer@studenti.unipd.it)
	* Riccardo Riva (riccardo.riva.1@studenti.unipd.it)
    * Riccardo Smerilli (riccardo.smerilli@studenti.unipd.it)
    * Mukesh Tirupathi (mukesh.tirupathi@studenti.unipd.it)

### How do I get set up? ###

* Open the project using the pom.xml file. It already contains all the dependencies and related functions to propely compile the project

* Put the document corpora as JSON file in the "experiment/documents" folder

* On the maven tab first launch the clear command, once it has finished launch the  package  command

* Finally run the file on the terminal using the command: "java -cp target/se-datex-project-1.00-jar-with-dependencies.jar it.unipd.dei.se.MainClass"

* Once all the processes are terminated you will find in the outputh file with all the ranked lists on the "experiments/runs" directory

* All the index related files are stored in the "experiments/index" folder


### Who do I talk to? ###

* For information contact the group members
