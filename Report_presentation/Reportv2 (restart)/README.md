# Search Engines (SE) - Homework Templates

This repository contains templates for the reports on homeworks to be developed in the *Search Engines* course.

*Search Engines* is a course of the [Master Degree in Computer Engineering](https://lauree.dei.unipd.it/lauree-magistrali/computer-engineering/) of the  [Department of Information Engineering](https://www.dei.unipd.it/en/), [University of Padua](https://www.unipd.it/en/), Italy. 

*Search Engines* is part of the teaching activities of the [Intelligent Interactive Information Access (IIIA) Hub](http://iiia.dei.unipd.it/).
