package it.unipd.dei.se.TEST_ANALYZERS;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;

/**
 * This class is used to test the performance of the Whitespace Tokenizer only
 *
 *  TOKENIZER: WhitespaceTokenizer
 *
 *  FILTERS: 1) LowerCaseFilter
 *
 *  RESULTS: 1) BM25Similarity -> num_rel_ret = 371
 *                             -> set_F = 0.1318
 *           2) BooleanSimilarity -> num_rel_ret = 132
 *                                -> set_F = 0.0600
 *
 */
public class TOKENIZERS_TEST_WhitespaceTokenizer extends Analyzer {


    /**
     * Creates a new instance of the analyzer.
     */
    public TOKENIZERS_TEST_WhitespaceTokenizer() {
        super();
    }

    /**
     * Here we put the tokenizer and all the token filters we want to implement
     */
    @Override
    protected TokenStreamComponents createComponents(String s) {

        //Tokenizer
        final Tokenizer source = new WhitespaceTokenizer();

        //Filters
        TokenStream tokens = new LowerCaseFilter(source);


        return new TokenStreamComponents(source, tokens);
    }


}
