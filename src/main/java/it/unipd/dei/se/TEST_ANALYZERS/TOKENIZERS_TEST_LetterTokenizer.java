package it.unipd.dei.se.TEST_ANALYZERS;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LetterTokenizer;

/**
 * This class is used to test the performance of the Letter Tokenizer only
 *
 *  TOKENIZER: LetterTokenizer
 *
 *  FILTERS: 1) LowerCaseFilter
 *
 *  RESULTS: 1) BM25Similarity -> num_rel_ret = 404
 *                             -> set_F = 0.1427
 *           2) BooleanSimilarity -> num_rel_ret = 158
 *                                -> set_F = 0.0708
 *
 */
public class TOKENIZERS_TEST_LetterTokenizer extends Analyzer {


    /**
     * Creates a new instance of the analyzer.
     */
    public TOKENIZERS_TEST_LetterTokenizer() {
        super();
    }

    /**
     * Here we put the tokenizer and all the token filters we want to implement
     */
    @Override
    protected TokenStreamComponents createComponents(String s) {

        //Tokenizer
        final Tokenizer source = new LetterTokenizer();

        //Filters
        TokenStream tokens = new LowerCaseFilter(source);


        return new TokenStreamComponents(source, tokens);
    }


}
