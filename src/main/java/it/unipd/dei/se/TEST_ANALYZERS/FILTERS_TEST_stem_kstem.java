package it.unipd.dei.se.TEST_ANALYZERS;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.en.KStemFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

/**
 * This class is used to test the performance of filters
 *
 *  TOKENIZER: StandardTokenizer
 *
 *  FILTERS: 1) LowerCaseFilter
 *
 *  RESULTS: num_rel_ret =
 *
 */
public class FILTERS_TEST_stem_kstem extends Analyzer {


    /**
     * Creates a new instance of the analyzer.
     */
    public FILTERS_TEST_stem_kstem() {
        super();
    }

    /**
     * Here we put the tokenizer and all the token filters we want to implement
     */
    @Override
    protected TokenStreamComponents createComponents(String s) {

        //Tokenizer
        final Tokenizer source = new StandardTokenizer();

        //Filters
        TokenStream tokens = new LowerCaseFilter(source);

        tokens = new KStemFilter(tokens);

        return new TokenStreamComponents(source, tokens);
    }


}
