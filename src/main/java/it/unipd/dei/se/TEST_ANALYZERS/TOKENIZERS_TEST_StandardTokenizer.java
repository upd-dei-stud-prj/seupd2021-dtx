package it.unipd.dei.se.TEST_ANALYZERS;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;

/**
 * This class is used to test the performance of the Standard Tokenizer only
 *
 *  TOKENIZER: StandardTokenizer
 *
 *  FILTERS: 1) LowerCaseFilter
 *
 *  RESULTS: 1) BM25Similarity -> num_rel_ret = 406
 *                             -> set_F = 0.1434
 *           2) BooleanSimilarity -> num_rel_ret = 156
 *                                -> set_F = 0.0701
 *
 */
public class TOKENIZERS_TEST_StandardTokenizer extends Analyzer {


    /**
     * Creates a new instance of the analyzer.
     */
    public TOKENIZERS_TEST_StandardTokenizer() {
        super();
    }

    /**
     * Here we put the tokenizer and all the token filters we want to implement
     */
    @Override
    protected TokenStreamComponents createComponents(String s) {

        //Tokenizer
        final Tokenizer source = new StandardTokenizer();

        //Filters
        TokenStream tokens = new LowerCaseFilter(source);


        return new TokenStreamComponents(source, tokens);
    }


}
