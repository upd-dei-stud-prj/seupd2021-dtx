package it.unipd.dei.se.TEST_ANALYZERS;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import static it.unipd.dei.se.analyzer.AnalyzerUtil.loadStopList;

/**
 * This class is used to test the performance of filters
 *
 *  TOKENIZER: StandardTokenizer
 *
 *  FILTERS: 1) LowerCaseFilter
 *           2) StopFilter -> smart.txt
 *
 *  RESULTS: num_rel_ret = 404
 *           set_F = 0.1412
 *
 */
public class FILTERS_TEST_stop_smart extends Analyzer {


    /**
     * Creates a new instance of the analyzer.
     */
    public FILTERS_TEST_stop_smart() {
        super();
    }

    /**
     * Here we put the tokenizer and all the token filters we want to implement
     */
    @Override
    protected TokenStreamComponents createComponents(String s) {

        //Tokenizer
        final Tokenizer source = new StandardTokenizer();

        //Filters
        TokenStream tokens = new LowerCaseFilter(source);

        tokens = new StopFilter(tokens, loadStopList("smart.txt"));



        return new TokenStreamComponents(source, tokens);
    }


}
