package it.unipd.dei.se.analyzer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishMinimalStemFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import static it.unipd.dei.se.analyzer.AnalyzerUtil.loadStopList;

public class DatexAnalyzer extends Analyzer {


    /**
     * Creates a new instance of the analyzer.
     */
    public DatexAnalyzer() {
        super();
    }

    /**
     * Here we put the tokenizer and all the token filters we want to implement
     */
    @Override
    protected TokenStreamComponents createComponents(String s) {

        /**** First set of filters (atire stopwords)
         * runid                   all     EngStem_atire
         * num_q                   all     49
         * num_ret                 all     4796
         * num_rel                 all     932
         * num_rel_ret             all     424
         * utility                 all     -80.5714
         * set_P                   all     0.0884
         * set_relative_P          all     0.5139
         * set_recall              all     0.5139
         * set_map                 all     0.0497
         * set_F                   all     0.1479
         *

        //Tokenizer
        final Tokenizer source = new StandardTokenizer();

        //Filters
        TokenStream tokens = new LowerCaseFilter(source);

        tokens = new StopFilter(tokens, loadStopList("atire.txt"));

        tokens = new EnglishMinimalStemFilter(tokens);***/



        /*** First set of filters (lingpipe stopwords)
         * runid                   all     EngStem_lingpipe
         * num_q                   all     49
         * num_ret                 all     4779
         * num_rel                 all     932
         * num_rel_ret             all     433
         * utility                 all     -79.8571
         * set_P                   all     0.0906
         * set_relative_P          all     0.5233
         * set_recall              all     0.5233
         * set_map                 all     0.0509
         * set_F                   all     0.1513
         *

        //Tokenizer
        final Tokenizer source = new StandardTokenizer();

        //Filters
        TokenStream tokens = new LowerCaseFilter(source);

        tokens = new StopFilter(tokens, loadStopList("lingpipe.txt"));

        tokens = new EnglishMinimalStemFilter(tokens);***/




        /**** Second set of filters (atire stopwords)
         * runid                   all     doubleStem_atire
         * num_q                   all     49
         * num_ret                 all     4814
         * num_rel                 all     932
         * num_rel_ret             all     429
         * utility                 all     -80.7347
         * set_P                   all     0.0891
         * set_relative_P          all     0.5205
         * set_recall              all     0.5205
         * set_map                 all     0.0503
         * set_F                   all     0.1492
         *
        //Tokenizer
        final Tokenizer source = new StandardTokenizer();

        //Filters
        TokenStream tokens = new LowerCaseFilter(source);

        tokens = new StopFilter(tokens, loadStopList("atire.txt"));

        tokens = new EnglishMinimalStemFilter(tokens);
        tokens = new LovinsStemFilter(tokens);***/



        /**** Second set of filters (lingpipe stopwords) BEST ONE
         * runid                   all     doubleStem_lingpipe
         * num_q                   all     49
         * num_ret                 all     4758
         * num_rel                 all     932
         * num_rel_ret             all     441
         * utility                 all     -79.1020
         * set_P                   all     0.0926
         * set_relative_P          all     0.5224
         * set_recall              all     0.5224
         * set_map                 all     0.0513
         * set_F                   all     0.1542
         *
         ****/
        //Tokenizer
        final Tokenizer source = new StandardTokenizer();

        //Filters
        TokenStream tokens = new LowerCaseFilter(source);

        tokens = new StopFilter(tokens, loadStopList("lingpipe.txt"));

        tokens = new EnglishMinimalStemFilter(tokens);
        tokens = new LovinsStemFilter(tokens);


        /**** Third set of filters (lovins stem)
         * runid                   all     LovinsStem_lingpipe
         * num_q                   all     49
         * num_ret                 all     4754
         * num_rel                 all     932
         * num_rel_ret             all     437
         * utility                 all     -79.1837
         * set_P                   all     0.0918
         * set_relative_P          all     0.5155
         * set_recall              all     0.5155
         * set_map                 all     0.0505
         * set_F                   all     0.1527
         *

        //Tokenizer
        final Tokenizer source = new StandardTokenizer();

        //Filters
        TokenStream tokens = new LowerCaseFilter(source);

        tokens = new StopFilter(tokens, loadStopList("lingpipe.txt"));

        tokens = new LovinsStemFilter(tokens);****/



        /**** Fourth set of filters (person entity)
         * runid                   all     personEntity
         * num_q                   all     49
         * num_ret                 all     4758
         * num_rel                 all     932
         * num_rel_ret             all     441
         * utility                 all     -79.1020
         * set_P                   all     0.0926
         * set_relative_P          all     0.5224
         * set_recall              all     0.5224
         * set_map                 all     0.0513
         * set_F                   all     0.1542
         *
         *
         //Tokenizer
         final Tokenizer source = new StandardTokenizer();

         //Filters
         TokenStream tokens = new LowerCaseFilter(source);

         tokens = new StopFilter(tokens, loadStopList("lingpipe.txt"));

         tokens = new EnglishMinimalStemFilter(tokens);

         tokens = new LovinsStemFilter(tokens);

         tokens = new OpenNLPNERFilter(tokens, loadLNerTaggerModel("en-ner-person.bin"));***/



        /**** Fifth set of filters (CommonGrams)
         * runid                   all     common_grams_stem
         * num_q                   all     49
         * num_ret                 all     4514
         * num_rel                 all     932
         * num_rel_ret             all     275
         * utility                 all     -80.8980
         * set_P                   all     0.0607
         * set_relative_P          all     0.3354
         * set_recall              all     0.3354
         * set_map                 all     0.0273
         * set_F                   all     0.1007
         *

        final Tokenizer source = new WhitespaceTokenizer();

        TokenStream tokens = new LowerCaseFilter(source);

        tokens = new EnglishMinimalStemFilter(tokens);
        tokens = new LovinsStemFilter(tokens);

        tokens = new CommonGramsFilter(tokens,loadStopList("lingpipe.txt"));

        tokens = new StopFilter(tokens, loadStopList("lingpipe.txt"));***/






        return new TokenStreamComponents(source, tokens);
    }


}
