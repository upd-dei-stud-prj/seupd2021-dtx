package it.unipd.dei.se;

import it.unipd.dei.se.TEST_ANALYZERS.*;
import it.unipd.dei.se.analyzer.DatexAnalyzer;
import it.unipd.dei.se.indexer.DirectoryIndexer;
import it.unipd.dei.se.parser.Parser;
import it.unipd.dei.se.searcher.Searcher;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;

import java.io.IOException;

/**
 * Search Engines Project of Datex Group
 *
 * @author
 * @author
 * @author Emanuela Pozzer (emanuela.pozzer@studenti.unipd.it)
 * @author
 * @author
 * @author Riccardo Riva (riccardo.riva.1@studenti.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class MainClass {

    //Useful
    private static final String DOCS_PATH = "experiments/documents";
    private static final String TOPICS_PATH = "experiments/topics/topics.xml";
    private static final String INDEX_PATH = "experiments/index";
    private static final String RUNS_PATH = "experiments/runs";
    private static final String RUN_ID = "FILTERS_TEST_commong_grams_stem";
    private static final String CHAR_SET_NAME = "ISO-8859-1";
    private static final String FILE_EXTENSION = "json";
    private static final int MAX_RETRIEVED_DOCUMENTS = 100;
    private static final int RAM_BUFFER_SIZE = 1024;

    //Useless
    private static final int EXPECTED_DOCUMENTS = 100;
    private static final int EXPECTED_TOPICS = 50;



    public static void main(String[] args) throws IOException, ParseException {

        System.out.printf("##################################\n");
        System.out.printf("#        SE Datex Project!       #\n");
        System.out.printf("##################################\n");

        if(args.length < 2)
        {
            System.out.println("Missing args info. Put: 1) document corpora path 2) run.txt path \n The folder must already exist");
            return;
        }

        String docs_and_topics_path = args[0];
        
        String run_path = args[1];

        //Define the custom analyzer
        final DatexAnalyzer datexAnalyzer = new DatexAnalyzer();

        //Define similarities type
        final Similarity similarity = new BM25Similarity();

        //Define the indexer properties
        final DirectoryIndexer Index = new DirectoryIndexer(datexAnalyzer, similarity, RAM_BUFFER_SIZE,
                INDEX_PATH, docs_and_topics_path, FILE_EXTENSION, CHAR_SET_NAME,
                EXPECTED_DOCUMENTS, Parser.class);

        //Compute the index
        Index.index();

        //Init searcher
        final Searcher s = new Searcher(datexAnalyzer, similarity, INDEX_PATH, docs_and_topics_path + "/topics.xml", EXPECTED_TOPICS, RUN_ID, run_path, MAX_RETRIEVED_DOCUMENTS);

        //Compute run file
        s.search();

    }

}
